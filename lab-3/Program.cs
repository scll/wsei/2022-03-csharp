﻿using System;
using System.Collections.Generic;

namespace lab_3
{
    class App
    {
        public static void Main(string[] args)
        {
            //UWAGA!!! Nie usuwaj poniższego wiersza!!!
            //Console.WriteLine("Otrzymałeś punktów: " + (Test.Exercises_1() + Test.Excersise_2() + Test.Excersise_3()));
            //Keyboard kbrd = new Keyboard();
            Musician<Keyboard> m = new Musician<Keyboard>();
            Console.WriteLine(m.ToString());
            ValueTuple<string, int, bool> tuple1 = Exercise2.getTuple1();
            Console.WriteLine(tuple1.ToString());

            int[] arr = { 2, 3, 4, 2 };
            ValueTuple<int[], bool> tuple = Exercise2.GetTuple2<int>(arr);
            Console.WriteLine(tuple.Item2.ToString());

            string[] arr2 = { "adam", "ola", "adam", "ewa", "karol", "ala", "adam", "ola" };
            //wywołanie metody
            (string, int)[] arr3 = Exercise3.countElements(arr2, "adam", "ewa", "ola");
            foreach (var element in arr3)
            {
                Console.WriteLine(element.Item1);
                Console.WriteLine(element.Item2);

            }
        }
    }

    //Ćwiczenie 1
    //Zmodyfikuj klasę Musician, aby można było tworzyć muzyków dla T  pochodnych po Instrument.
    //Zdefiniuj klasę  Guitar pochodną po Instrument, w metodzie Play zwróć łańcuch "GUITAR";
    //Zdefiniuj klasę Drum pochodną po Instrument, w metodzie Play zwróć łańcuch "DRUM";
    interface IPlay
    {
        string Play();
    }

    class Musician<T> : IPlay where T : Instrument, new()
    {
        public T Instrument { get; init; }

        public string Play()
        {
            return (Instrument)?.Play();
        }
        public Musician()
        {
            Instrument = new T();
        }

        public override string ToString()
        {
            return $"MUSICIAN with {(Instrument)?.Play()}";
        }
    }

    abstract class Instrument : IPlay
    {
        public abstract string Play();
    }

    class Keyboard : Instrument
    {
        public override string Play()
        {
            return "KEYBOARD";
        }
    }

    class Guitar : Instrument
    {
        public override string Play()
        {
            return "GUITAR";
        }
    }

    class Drum : Instrument
    {
        public override string Play()
        {
            return "DRUM";
        }
    }

    //Cwiczenie 2
    public class Exercise2
    {
        //Zmień poniższą metodę, aby zwracała krotkę z polami typu string, int i bool oraz wartościami "Karol", 12 i true
        public static ValueTuple<string, int, bool> getTuple1()
        {
            return ValueTuple.Create("Karol", 12, true);
        }

        //Zdefiniuj poniższą metodę, aby zwracała krotkę o dwóch polach
        //firstAndLast: z tablicą dwuelementową z pierwszym i ostatnim elementem tablicy input
        //isSame: z wartością logiczną określająca równość obu elementów
        //dla pustej tablicy należy zwrócić krotkę z pustą tablica i wartościa false
        //Przykład
        //dla wejścia
        //int[] arr = {2, 3, 4, 6}
        //metoda powinna zwrócić krotkę
        //var tuple = GetTuple2<int>(arr);
        //tuple.firstAndLast    ==> {2, 6}
        //tuple.isSame          ==> false
        public static ValueTuple<T[], bool> GetTuple2<T>(T[] arr)
        {
            T[] firstLast = new T[2] { arr[0], arr[^1] };
            (T[] firstAndLast, bool isSame) tuple = ValueTuple.Create(firstLast, EqualityComparer<T>.Default.Equals(arr[0], arr[^1]));
            return tuple;
        }
    }

    //Cwiczenie 3
    public class Exercise3
    {
        //Zdefiniuj poniższa metodę, która zlicza liczbę wystąpień elementów tablicy arr
        //Przykład
        //dla danej tablicy
        //string[] arr = {"adam", "ola", "adam" ,"ewa" ,"karol", "ala" ,"adam", "ola"};
        //wywołanie metody
        //countElements(arr, "adam", "ewa", "ola");
        //powinno zwrócić tablicę krotek
        //{("adam", 3), ("ewa", 1), ("ola", 2)}
        //co oznacza, że "adam" występuje 3 raz, "ewa" 1 raz a "ola" 2
        //W obu tablicach moga pojawić się wartości null, które też muszą być zliczane
        public static (T, int)[] countElements<T>(T[] arr, params T[] elements)
        {
            (T, int)[] tupleArray = new (T, int)[elements.Length];

            int i = 0;
            foreach (var element in elements)
            {
                (T, int) tuple = ValueTuple.Create(element, 0);

                foreach (var item in arr)
                {
                    if(EqualityComparer<T>.Default.Equals(item, element))
                    {
                        tuple = ValueTuple.Create(tuple.Item1, tuple.Item2 + 1);
                    }
                }
                tupleArray[i] = tuple;
                i++;
            }

            return tupleArray;
        }
    }
}
