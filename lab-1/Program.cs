﻿using System;
using System.Linq;

namespace lab_1
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateTime = DateTime.Parse("03-02-2022");
            Console.WriteLine(dateTime);
            DateTime newDate = dateTime.AddDays(291);
            Console.WriteLine(newDate);

            Money money1 = Money.Of(10, Currency.PLN);
            Money money2 = Money.Of(21, Currency.PLN);
            Money mnozenie = money1 * 6;
            Console.WriteLine(mnozenie.Value);
            Money dodawanie = money1 + money2;
            Console.WriteLine(dodawanie.Value);

            Console.WriteLine("-----------");

            Tank tank1 = new Tank(20);
            tank1.refuel(10);
            Tank tank2 = new Tank(30);
            tank2.refuel(15);
            Console.WriteLine(tank1.ToString());
            Console.WriteLine(tank2.ToString());
            tank1.refuel(tank2, 8);
            Console.WriteLine(tank1.ToString());
            Console.WriteLine(tank2.ToString());

            Console.WriteLine("-----------");

            Student[] students =
            {
                new Student("Student1", "Name2", 4),
                new Student("Student1", "Name1", 3),
                new Student("Student2", "Name1", 2),
                new Student("Student3", "Name4", 3),
                new Student("Student3", "Name4", 2),
                new Student("Student3", "Name3", 2),
            };
            Array.Sort(students);
            students.ToList().ForEach(a => Console.WriteLine(a));
            Console.WriteLine("-----------");


        }

        public enum Currency
        {
            PLN = 1,
            USD = 2,
            EUR = 3
        }
        public class Money
        {
            private decimal _value;
            private Currency _currency;
            public decimal Value { get => _value; set => _value = value; }
            public Currency Currency { get => _currency; set => _currency = value; }

            private Money(decimal value, Currency currency)
            {
                _value = value;
                _currency = currency;
            }

            public static Money? Of(decimal value, Currency currency)
            {
                return value < 0 ? throw new Exception() : new Money(value, currency);
            }
            public static Money operator *(Money money, decimal factor)
            {
                return Of(money.Value * factor, money.Currency);
            }
            public static Money operator *(decimal factor, Money money)
            {
                return Of(money.Value * factor, money.Currency);
            }
            public static Money operator +(Money money1, Money money2)
            {
                return money1.Currency == money2.Currency ? Of(money1.Value + money2.Value, money1.Currency) : throw new Exception();
            }
            public static bool operator >(Money money1, Money money2)
            {
                return money1.Currency == money2.Currency ? money1.Value > money2.Value : throw new Exception();
            }
            public static bool operator <(Money money1, Money money2)
            {
                return money1.Currency == money2.Currency ? money1.Value < money2.Value : throw new Exception();
            }
            public static bool operator ==(Money money1, Money money2)
            {
                return money1.Value == money2.Value && money1.Currency == money2.Currency;
            }
            public static bool operator !=(Money money1, Money money2)
            {
                return money1.Value != money2.Value || money1.Currency != money2.Currency;
            }
            public static implicit operator decimal(Money money)
            {
                return money.Value;
            }
            public static explicit operator double(Money money)
            {
                return (double)money.Value;
            }
            public static explicit operator float(Money money)
            {
                return (float)money.Value;
            }
            public override string ToString()
            {
                return $"{Value} {Currency}";
            }

            public override bool Equals(object obj)
            {
                return obj is Money money &&
                       _value == money._value &&
                       _currency == money._currency;
            }
            public override int GetHashCode()
            {
                return HashCode.Combine(_value, _currency);
            }
        }
        public class Tank
        {
            public readonly int Capacity;
            private int _level; public Tank(int capacity)
            {
                Capacity = capacity;
            }
            public int Level
            {
                get
                {
                    return _level;
                }
                private set
                {
                    if (value < 0 || value > Capacity)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    _level = value;
                }
            }
            public bool refuel(int amount)
            {
                if (amount < 0)
                {
                    return false;
                }
                if (_level + amount > Capacity)
                {
                    return false;
                }
                Console.WriteLine("refueled");
                _level += amount;
                return true;
            }
            public bool consume(int amount)
            {
                if (amount < 0)
                {
                    return false;
                }
                if (_level - amount < 0)
                {
                    return false;
                }
                _level -= amount;
                return true;
            }
            public bool refuel(Tank sourceTank, int amount)
            {
                if (amount < 0)
                {
                    return false;
                }
                if (_level + amount > Capacity)
                {
                    return false;
                }
                else if (sourceTank.consume(amount))
                {
                    _level += amount;
                    return true;
                }
                return false;
            }
            public override string ToString()
            {
                return $"Tank: Capacity = {Capacity}, Level = {Level}";
            }
        }

        public class Student : IComparable<Student>
        {
            public string Surname { get; set; }
            public string Name { get; set; }
            public decimal Avg { get; set; }
            public Student(string surname, string name, decimal avg)
            {
                Surname = surname;
                Name = name;
                Avg = avg;
            }

            public int CompareTo(Student? other)
            {
                if (ReferenceEquals(this, other)) return 0;
                if (ReferenceEquals(null, other)) return 1;
                var surnameComparison = Surname.CompareTo(other.Surname);
                var nameComparison = Name.CompareTo(other.Name);
                var avgComparison = Avg.CompareTo(other.Avg);
                if (surnameComparison != 0) return surnameComparison;
                else if (nameComparison != 0) return nameComparison;
                return avgComparison;
            }
            public override string ToString()
            {
                return $"{Surname} {Name} {Avg}";
            }
        }
    }
}
