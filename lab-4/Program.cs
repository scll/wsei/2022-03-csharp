﻿using System;
using System.Diagnostics.Tracing;
using System.Drawing;

namespace lab_4
{

    class App
    {
        public static void Main(string[] args)
        {

            //(int, int) point1 = (2, 4);
            //(int, int) screensize = (4, 4);
            //Direction4 dir = Direction4.RIGHT;
            //var point2 = Exercise1.NextPoint(dir, point1, screensize);
            //Console.WriteLine(point2);
            //w point2 powinny być wartości(2, 3);


            //int[,] screen =
            //{
            //    {1, 0, 0},
            //    {0, 3, 0},
            //    {0, 0, 0}
            // };
            //(int, int) point = (1, 1);
            //Console.WriteLine(Exercise2.DirectionTo(screen, point, 1));
            // w direction powinna znaleźć się stała UP_LEFT


            //Student[] students = {
            //  new Student("Kowal","Adam", 'A'),
            //  new Student("Nowak","Ewa", 'A')
            //};
            //Exercise4.AssignStudentId(students);
            //w tablicy students otrzymamy:
            // Kowal Adam 'A' - 'A001'
            // Nowal Ewa 'A'  - 'A002'
        }
    }

    enum Direction8
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        UP_LEFT,
        DOWN_LEFT,
        UP_RIGHT,
        DOWN_RIGHT
    }

    enum Direction4
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    //Cwiczenie 1
    //Zdefiniuj metodę NextPoint, która powinna zwracać krotkę ze współrzędnymi piksela przesuniętego jednostkowo w danym kierunku względem piksela point.
    //Krotka screenSize zawiera rozmiary ekranu (width, height)
    //Przyjmij, że początek układu współrzednych (0,0) jest w lewym górnym rogu ekranu, a prawy dolny ma współrzęne (witdh, height) 
    //Pzzykład
    //dla danych wejściowych 
    //(int, int) point1 = (2, 4);
    //Direction4 dir = Direction4.UP;
    //var point2 = NextPoint(dir, point1);
    //w point2 powinny być wartości (2, 3);
    //Jeśli nowe położenie jest poza ekranem to metoda powinna zwrócić krotkę z point
    class Exercise1
    {
        public static (int, int) NextPoint(Direction4 direction, (int, int) point, (int, int) screenSize)
        {
            switch (direction)
            {
                case Direction4.UP:
                    if (point.Item2 - 1 >= 0)
                        return ValueTuple.Create(point.Item1, point.Item2 - 1);
                    return point;
                case Direction4.DOWN:
                    if (point.Item2 + 1 <= screenSize.Item2)
                        return ValueTuple.Create(point.Item1, point.Item2 + 1);
                    return point;
                case Direction4.LEFT:
                    if (point.Item1 - 1 >= 0)
                        return ValueTuple.Create(point.Item1 - 1, point.Item2);
                    return point;
                case Direction4.RIGHT:
                    if (point.Item1 + 1 <= screenSize.Item1)
                        return ValueTuple.Create(point.Item1 + 1, point.Item2);
                    return point;
                default:
                    return point;
            }
        }
    }
    //Cwiczenie 2
    //Zdefiniuj metodę DirectionTo, która zwraca kierunek do piksela o wartości value z punktu point. W tablicy screen są wartości
    //pikseli. Początek układu współrzędnych (0,0) to lewy górny róg, więc punkt o współrzęnych (1,1) jest powyżej punktu (1,2) 
    //Przykład
    // Dla danych weejsciowych
    // static int[,] screen =
    // {
    //    {1, 0, 0},
    //    {0, 0, 0},
    //    {0, 0, 0}
    // };
    // (int, int) point = (1, 1);
    // po wywołaniu - Direction8 direction = DirectionTo(screen, point, 1);
    // w direction powinna znaleźć się stała UP_LEFT
    class Exercise2
    {
        static int[,] screen =
        {
            {1, 0, 0},
            {0, 0, 0},
            {0, 0, 0}
        };

        private static (int, int) point = (1, 1);

        private Direction8 direction = DirectionTo(screen, point, 1);

        public static Direction8 DirectionTo(int[,] screen, (int, int) point, int value)
        {
            int point_x = point.Item1;
            int point_y = point.Item2;
            for (int i = 0; i < screen.GetLength(0); i++)
            {
                for (int j = 0; j < screen.GetLength(1); j++)
                {
                    if (screen[i, j] == value)
                    {
                        if (j < point_x && i == point_y)
                        {
                            return Direction8.LEFT;
                        }
                        if (j > point_x && i == point_y)
                        {
                            return Direction8.RIGHT;
                        }

                        if (j == point_x && i > point_y)
                        {
                            return Direction8.UP;
                        }
                        if (j == point_x && i == point_y)
                        {
                            return Direction8.DOWN;
                        }

                        if (j < point_x && i < point_y)
                        {
                            return Direction8.UP_LEFT;
                        }
                        if (j < point_x && i > point_y)
                        {
                            return Direction8.DOWN_LEFT;
                        }

                        if (j > point_x && i < point_y)
                        {
                            return Direction8.UP_RIGHT;
                        }
                        if (j > point_x && i > point_y)
                        {
                            return Direction8.DOWN_RIGHT;
                        }

                        break;
                    }
                }
            }
            return Direction8.UP;
        }
    }

    //Cwiczenie 3
    //Zdefiniuj metodę obliczającą liczbę najczęściej występujących aut w tablicy cars
    //Przykład
    //dla danych wejściowych
    // Car[] _cars = new Car[]
    // {
    //     new Car(),
    //     new Car(Model: "Fiat", true),
    //     new Car(),
    //     new Car(Power: 100),
    //     new Car(Model: "Fiat", true),
    //     new Car(Power: 125),
    //     new Car()
    // };
    //wywołanie CarCounter(Car[] cars) powinno zwrócić 3
    record Car(string Model = "Audi", bool HasPlateNumber = false, int Power = 100);

    class Exercise3
    {
        public static int CarCounter(Car[] cars)
        {
            throw new NotImplementedException();
        }
    }

    record Student(string LastName, string FirstName, char Group, string StudentId = "");
    //Cwiczenie 4
    //Zdefiniuj metodę AssignStudentId, która każdemu studentowi nadaje pole StudentId wg wzoru znak_grupy-trzycyfrowy-numer.
    //Przykład
    //dla danych wejściowych
    //Student[] students = {
    //  new Student("Kowal","Adam", 'A'),
    //  new Student("Nowak","Ewa", 'A')
    //};
    //po wywołaniu metody AssignStudentId(students);
    //w tablicy students otrzymamy:
    // Kowal Adam 'A' - 'A001'
    // Nowal Ewa 'A'  - 'A002'
    //Należy przyjąc, że są tylko trzy możliwe grupy: A, B i C
    class Exercise4
    {
        public static void AssignStudentId(Student[] students)
        {
            for (int i = 0; i < students.Length; i++)
            {
                var student_id = char.ToString(students[i].Group) + string.Format("{0:000}", i + 1);
                students[i] = new Student(students[i].LastName, students[i].FirstName, students[i].Group, student_id);
                Console.WriteLine(students[i].LastName + " " + students[i].FirstName + " '" + students[i].Group + "' - '" + students[i].StudentId + "' ");
            }
        }
    }
}
